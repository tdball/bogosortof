import random
import time
import sys
import multiprocessing as mp
from typing import List


def bogosort(random_integers: List[int], sorted_flag: mp.Event, quit: mp.Event, result: mp.Value) -> List[int]:
    """
    Accepts a list of integers, compares it to the sorted version of it's self.
    If sorted, it returns the list, if unsorted, it reshuffles, and compares again.
    """
    while not quit_flag.is_set():
        if random_integers != sorted(random_integers):
            random.shuffle(random_integers)
        else:
            for i in range(len(random_integers)):
                result[i] = random_integers[i]
            sorted_flag.set()

def get_input() -> int:
  """
  We will get an input number from the user and
  provide a confirmation for array lengths over 
  the value of 5 as they may exceed the recursion
  limit

  returns sanitized_length
  """

  try:
    length = input("\nPlease enter an array length: ")
    if length.lower() == 'q':
      sys.exit(0)
    sanitized_length = int(length)
  except SyntaxError:
    print("Please make sure to enter a number...")
    sanitized_length = get_input()
  except ValueError:
    print("Please make sure to enter an INTEGER")
    sanitized_length = get_input()

  return sanitized_length


if __name__ == '__main__':
    array_length = get_input()
    # Here we create the random array to be sorted
    random_integers = list(range(array_length))
    random.shuffle(random_integers)
    # Some state flags to allow cross-process communication (fancy booleans)
    sorted_flag = mp.Event()
    quit_flag = mp.Event()
    # Shared data amongst processes
    result = mp.Array('i', array_length)
    for cpu in range(mp.cpu_count()):
        process = mp.Process(target=bogosort, args=(random_integers, sorted_flag, quit_flag, result))
        process.start()
    # Wait for the sorted_flag to be set
    sorted_flag.wait()
    # Let all other processes know to terminate gracefully
    quit_flag.set()
    print(result[:], '\n')
    
        


# Bogosortof



Just writing this up as a sort of learning exercise. 

Parallel processing algorithm for bogosort.

Each process handles an individual stream of bogosorting goodness,
as soon as one process sorts the list, it signals to the others to close.
The results are then printed to the display. I generate a list of length
based on user input, and then shuffle that list in place, and pass it to
the sorting algorithm.

## Usage

Developed with Python 3.6

```bash
git clone https://gitlab.com/tdball/bogosortof.git
cd bogosortof
python3 bogo.py
```
This asks for an array length, I find 10 seems to be a nice
sweet spot for this terrible sorting algorithm. 
